# Recovery_Image

Python script to create a customized macOS Recovery Image

### Overview

All of the workflows/scripts that I've encountered to date that create a stand-alone Recovery Image require the macOS Installer application downloaded from the Mac Appstore.

This implementation uses the Recovery Partition that is created on a running Mac system as the source.

The BaseSystem.dmg file is used in both approaches;  This implementation mounts a Shadow Copy of the dmg, and customizes it.  It then creates a new dmg, ready for restoration on a thumb drive using either Disk Utility or the command line asr binary.

As implemented, this script auto launches Terminal.app.  The code can be modified to run other Apps, though required libraries and/or frameworks may need to be copied over in order for them to run.

Python support is included by default; other scripting languages such as Ruby can be added, but will require modification of the script to copy over the required support libraries/frameworks.

This script makes heavy use of the [sh module](http://amoffat.github.io/sh/), which is included.



### License 
The MIT License (MIT)

Copyright (c)  2018 Armando I. Rivera (AIR)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.