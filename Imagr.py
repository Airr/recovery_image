#!/usr/bin/env python
#===============================================================================
# Copyright (C) 2018 by Armando I. Rivera
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#===============================================================================
import glob, re, os
from sh import diskutil,clear,find,which,rsync,rm,mkdir,cp,asr,mount,hdiutil,chown,stat


def getRecoveryVolume():
    output = diskutil.list()
    if output.exit_code == 0:
        info =re.search('Recovery.+(disk.+)',str(output))
        if info:
            return info.group(1)

def getBaseSystem(vol_name):
    file_path = find('/Volumes/'+vol_name,'-iname','BaseSystem.dmg')
    return file_path

def banner(banner_text):
    banner_length = len(banner_text)+2
    print '\n{:*^60}'.format(banner_text.center(banner_length))

def mountVolume(diskid):
    output = diskutil.mount(diskid)
    if output.exit_code:
        exit('Failed to mount Recovery Partition')
    volname = re.search('Volume (.+) on', str(output))
    if volname:
        return getBaseSystem(volname.group(1)).strip()

def unmountVolume(diskid):
    output = diskutil.unmount(diskid)
    if output.exit_code:
        exit('Failed to unmount Recovery Partition')

def detachVolume(diskid):
    output = hdiutil.detach(diskid)
    if output.exit_code:
        exit('Failed to detach ' + diskid)        

  
def createShadowDrive(dmg_path,dmg_size):
    banner('Creating Temporary Image')
    if os.path.exists('tmp-base.shadow'): rm('tmp-base.shadow')
    hdiutil.resize('-size', dmg_size, dmg_path,'-shadow','tmp-base.shadow')
    hdiutil.attach(dmg_path,'-nobrowse', '-owners', 'on', '-shadow', 'tmp-base.shadow')
    diskutil.rename('/Volumes/OS X Base System', 'ASR')
    pass

def customize():

    # Copy Terminal Utilities
    banner('Copying Terminal Utilities')

    for item in ('clear','nano','du','df', 'python'):
        print "Copying {}...".format(item)
        cp( which(item), '/Volumes/ASR/usr/bin' )
    
    # Copy Required Libraries
    banner('Copying Required Libraries')
    
    for item in ('libffi', 'libexpat','libssl','libcrypto'):
        print "Copying {}...".format(item)
        lib = glob.glob('/usr/lib/' + item + '*.dylib')
        rsync('-aEh', lib, '/Volumes/ASR/usr/lib')

    # Copy Python Framework
    banner('Copying Python Framwork')

    rsync('-aEh', '/System/Library/Frameworks/Python.framework', '/Volumes/ASR/System/Library/Frameworks')

    # Set up Packages Folder
    rm("/Volumes/ASR/System/Installation/Packages")
    mkdir('-p','/Volumes/ASR/System/Installation/Packages/Extras')

    # Update rc.install file
    banner('Configuring Default Launcher')

    os.chmod('/Volumes/ASR/etc/rc.install',0666)
    with open('/Volumes/ASR/etc/rc.install') as rcFile:
        rc_contents = rcFile.read()
        updated_contents = re.sub('LAUNCH=.+','LAUNCH="/bin/echo"\n/System/Installation/Packages/Extras/rc.imaging\n\n',rc_contents)

    with open('/Volumes/ASR/etc/rc.install','wb') as rcFile:
        rcFile.write(updated_contents)
    os.chmod('/Volumes/ASR/etc/rc.install',0555)

    # Remove LaunchDaemons so this all works
    for item in 'com.apple.locationd.plist com.apple.lsd.plist com.apple.tccd.system.plist com.apple.ocspd.plist com.apple.InstallerProgress.plist'.split():
        rm(os.path.join('/Volumes/ASR/System/Library/LaunchDaemons',item))

    # Terminal App as Default Loader
    with open('/Volumes/ASR/System/Installation/Packages/Extras/rc.imaging','wb') as imgFile:
        imgFile.write('#!/bin/bash\n\nApplications/Utilities/Terminal.app/Contents/MacOS/Terminal\n\n/sbin/halt\n\n')
    os.chmod('/Volumes/ASR/System/Installation/Packages/Extras/rc.imaging',0555)

def finalize(dmg_name, dmg_path):
    banner('Creating Final Disk Image')
    
    final_dmg = dmg_name+'.dmg'

    if os.path.exists(final_dmg): rm(final_dmg)
    
    diskutil.rename('/Volumes/ASR', dmg_name)
    detachVolume('/Volumes/' + dmg_name)    
    output = hdiutil.convert('-format','UDZO', '-o', final_dmg, dmg_path, '-shadow', 'tmp-base.shadow')
    rm('tmp-base.shadow')
    if output.exit_code:
        exit("Failed to convert to final image.")

    cur_user=str( stat('-f%Su','/dev/console') )
    os.chmod(final_dmg,0644)
    chown(cur_user.strip(),final_dmg)
        
def main():
    recovery_partition = getRecoveryVolume()
    base_system_dmg=mountVolume(recovery_partition)
    createShadowDrive(base_system_dmg,'2G')
    customize()
    finalize('Imagr',base_system_dmg)
    unmountVolume(recovery_partition)
    banner('Disk Image Created (Imagr.dmg)')
    print '\n\n'


   

if __name__ == '__main__':
    if os.geteuid():
        exit("You need root privileges to run this script.\nPlease try again, this time using 'sudo'. Exiting.")

    main()